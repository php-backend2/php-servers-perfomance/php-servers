ARG PHP_IMAGE=php:8.1.25-alpine3.18

FROM composer:2.6.5 AS composer
FROM $PHP_IMAGE AS base

ENV TZ=UTC+3
ARG GID=1000
ARG UID=1000
ARG ENVIRONMENT_NAME=local
ARG PHP_SERVER=server

ARG RUNTIME_DEPS='zip libzip libpng libxml2 libjpeg-turbo libwebp freetype postgresql-libs postgresql-client'
ARG BUILD_DEPS='libzip-dev libpng-dev libjpeg-turbo-dev libwebp-dev libxml2-dev freetype-dev postgresql-dev'
ARG PHP_EXENSIONS='pcntl bcmath zip gd exif pgsql pdo_pgsql sockets'
ENV NODE_PATH '/home/$UID/.npm-global/lib/node_modules'

COPY ./php.ini $PHP_INI_DIR/php.ini
COPY ./opcache.ini $PHP_INI_DIR/conf.d/opcache.ini
COPY ./php-fpm.conf* $PHP_INI_DIR-fpm.d/www.conf

RUN set -ex; \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone; \
    \
    apk add --no-cache --virtual .runtime-deps $RUNTIME_DEPS $(if [ $ENVIRONMENT_NAME = 'local' ] && [[ $PHP_SERVER =~ "^(swoole|roadrunner)$" ]]; then echo 'nodejs npm'; fi) $(if [[ $PHP_SERVER =~ "^(php-fpm)$" ]]; then echo 'fcgi'; fi); \
    apk add --no-cache --virtual .phpize_deps $PHPIZE_DEPS $BUILD_DEPS; \
    \
    if getent passwd noroot; then deluser noroot; fi; \
    if getent group noroot; then delgroup noroot; fi; \
    addgroup -g $GID --system noroot && adduser -s noroot -G noroot -D -u $UID noroot; \
    \
    docker-php-ext-install -j$(nproc) $PHP_EXENSIONS; \
    docker-php-ext-configure gd --enable-gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ --with-webp; \
    docker-php-ext-configure pcntl --enable-pcntl; \
    export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS"; \
    \
    pecl update-channels; \
    pecl install -o -f redis && docker-php-ext-enable redis; \
    if [[ $PHP_SERVER =~ "^(swoole|roadrunner)$" ]]; then \
      pecl install -o -f openswoole && docker-php-ext-enable openswoole; \
    fi; \
    \
    if [ $ENVIRONMENT_NAME = 'local' ] && [[ $PHP_SERVER =~ "^(swoole|roadrunner)$" ]]; then \
        mkdir -p /home/$UID/.npm-global/; \
        npm config set prefix '/home/$UID/.npm-global/'; \
        npm install -g chokidar; \
        chown -R $UID:$GID /home/$UID/.npm-global/; \
        npm cache clean --force; \
    fi; \
    apk del .phpize_deps && rm -rf /tmp/pear && apk cache clean && docker-php-source delete;

USER $UID

WORKDIR /app

COPY --chown=$UID:$GID --from=composer /usr/bin/composer /usr/bin/composer
COPY --chown=$UID:$GID . ./

FROM base as server

RUN composer install \
      --no-interaction \
      --no-progress \
      --no-autoloader \
      --no-cache \
      --ansi \
      --no-scripts \
      $(if [ $DEBUG == 'false' ]; then echo '--no-dev'; fi); \
    composer dump-autoload --optimize --strict-psr --apcu;

FROM base as server-local

ENTRYPOINT ["/entrypoint.sh"]

FROM server as swoole

CMD ["sh", "-c", "php artisan optimize && php artisan migrate --seed --force && php artisan octane:start --host=0.0.0.0 --port=9000 --workers=8 --task-workers=4 --max-requests=10000"]

FROM server as roadrunner

CMD ["sh", "-c", "php artisan optimize && php artisan migrate --seed --force && php artisan octane:start --host=0.0.0.0 --port=9000 --workers=8 --max-requests=10000"]

FROM server as php-fpm

CMD ["sh", "-c", "php artisan optimize && php artisan migrate --seed --force && php-fpm --nodaemonize"]

FROM nginxinc/nginx-unprivileged:1.24-perl as nginx

ARG UID=1000
ARG GID=1000

RUN rm -rf /etc/nginx/conf.d/default.conf
COPY --chown=$UID:$GID ./nginx.conf /etc/nginx/conf.d/laravel_server_block.conf

USER $UID

WORKDIR /app/public

COPY --chown=$UID:$GID --from=base /app/public /app/public
