version: '3.8'

volumes:
  minio_data: { }
  db_data: { }
  prometheus_data: { }

x-backend-healthcheck: &backend-service-healthcheck
  test: php artisan octane:status || exit 1
  interval: 10s
  timeout: 5s
  retries: 3
  start_period: 20s

x-backend-env: &backend-env
  COMPOSER_HOME: /app/storage/cache/composer
  APP_NAME: php-servers
  APP_ENV: local
  APP_KEY: base64:eRiGpW9DGgAASUx9UyoDpsQeoHxVJ8avpjcGlzdejEc=
  APP_DEBUG: true
  APP_URL: http://localhost:9000

  LOG_CHANNEL: stderr
  LOG_LEVEL: debug

  DB_CONNECTION: pgsql
  DB_HOST: database
  DB_PORT: 5432
  DB_USERNAME: ${DB_USERNAME:?DB_USERNAME}
  DB_PASSWORD: ${DB_PASSWORD:?DB_PASSWORD}
  DB_DATABASE: ${DB_DATABASE:?DB_DATABASE}

  DADATA_SECRET: ${DADATA_SECRET:?DADATA_SECRET}
  DADATA_TOKEN: ${DADATA_TOKEN:?DADATA_TOKEN}

x-backend-service: &backend-service
  build: &backend-service-build
    dockerfile: ../../build/laravel.Dockerfile
    context: ./src/${BACKEND_SRC_DIR:?BACKEND_SRC_DIR}
    target: ${BACKEND_BUILD_TARGET:?BACKEND_BUILD_TARGET}
    args: &backend-service-build-args
      UID: ${UID:-1000}
      GID: ${GID:-1000}
      PHP_SERVER: ${BACKEND_PHP_SERVER:?BACKEND_PHP_SERVER}
      DOCKER_BUILDKIT: 1
      ENVIRONMENT_NAME: ${ENVIRONMENT_NAME:-server}
  environment: *backend-env
  healthcheck: *backend-service-healthcheck
  restart: unless-stopped
  depends_on:
    - database

x-nginx-service: &nginx-service
  build:
    <<: *backend-service-build
    target: nginx
  restart: unless-stopped
  ports:
    - "0.0.0.0:8000:8080"

services:
  backend-swoole:
    <<: *backend-service
    environment:
      <<: *backend-env
      OCTANE_SERVER: swoole
    profiles:
      - swoole

  nginx-swoole:
    <<: *nginx-service
    depends_on:
      - backend-swoole
    profiles:
      - swoole

  backend-roadrunner:
    <<: *backend-service
    environment:
      <<: *backend-env
      OCTANE_SERVER: roadrunner
    profiles:
      - roadrunner

  nginx-roadrunner:
    <<: *nginx-service
    depends_on:
      - backend-roadrunner
    profiles:
      - roadrunner

  backend-php-fpm:
    <<: *backend-service
    build:
      <<: *backend-service-build
      args:
        <<: *backend-service-build-args
        PHP_IMAGE: php:8.1.25-fpm-alpine3.18
    healthcheck:
      <<: *backend-service-healthcheck
      test: SCRIPT_NAME=/ping SCRIPT_FILENAME=/ping REQUEST_METHOD=GET cgi-fcgi -bind -connect 127.0.0.1:9000 || exit 1
    profiles:
      - php-fpm

  nginx-php-fpm:
    <<: *nginx-service
    depends_on:
      - backend-php-fpm
    profiles:
      - php-fpm

  database:
    image: postgres:14-alpine3.18
    healthcheck:
      test: pg_isready --username=$${POSTGRES_USER?} --dbname=$${POSTGRES_DB?}
      interval: 3s
      timeout: 3s
      retries: 8
    environment:
      POSTGRES_ROOT_PASSWORD: ${DB_ROOT_PASSWORD:?DB_ROOT_PASSWORD}
      POSTGRES_USER: ${DB_USERNAME:?DB_USERNAME}
      POSTGRES_PASSWORD: ${DB_PASSWORD:?DB_PASSWORD}
      POSTGRES_DB: ${DB_DATABASE:?DB_DATABASE}
    restart: unless-stopped
    volumes:
      - db_data:/var/lib/postgresql/data
    profiles:
      - general

  prometheus:
    image: prom/prometheus:v2.48.1
    volumes:
      - ./src/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - prometheus_data:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--storage.tsdb.retention.time=200h'
      - '--web.console.libraries=/etc/prometheus/console_libraries'
      - '--web.console.templates=/etc/prometheus/consoles'
      - '--web.enable-lifecycle'
      - '--web.route-prefix=/'
    restart: unless-stopped
    ports:
      - '0.0.0.0:9090:9090'
    profiles:
      - monitoring

  node-exporter:
    image: prom/node-exporter:v1.7.0
    restart: unless-stopped
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)'
    profiles:
      - monitoring

  cadvisor:
    image: gcr.io/cadvisor/cadvisor:v0.46.0
    privileged: true
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
    labels:
      org.label-schema.group: "monitoring"
    profiles:
      - monitoring
# for MacOS see: https://github.com/google/cadvisor/issues/2838#issuecomment-1248819708
#    devices:
#      - /dev/kmsg:/dev/kmsg
#    volumes:
#      - /var/run:/var/run:ro
#      - /sys:/sys:ro
#      - /var/lib/docker/:/var/lib/docker:ro
#      - /var/run/docker.sock:/var/run/docker.sock:ro
#      - /etc/machine-id:/etc/machine-id:ro
#      - /var/lib/dbus/machine-id:/var/lib/dbus/machine-id:ro
