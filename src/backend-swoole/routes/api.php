<?php

declare(strict_types=1);

use App\Http\Controllers\GetNearestPostOfficeAction;
use App\Http\Controllers\ListSockColorController;
use App\Http\Controllers\ListUserController;
use App\Http\Controllers\ListUserSockController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('users/{limit}', ListUserController::class)->name('list-users');
Route::get('user-socks/{limit}', ListUserSockController::class)->name('list-user-socks');
Route::get('sock-colors/{limit}', ListSockColorController::class)->name('list-sock-colors');
Route::get('nearest-post-office', GetNearestPostOfficeAction::class)
    ->name('nearest-post-office');
