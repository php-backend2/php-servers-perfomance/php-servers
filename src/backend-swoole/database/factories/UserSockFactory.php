<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserSock>
 */
class UserSockFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $additionalNames = [];

        foreach (range(1,rand(10,250)) as $i) {
            $additionalNames[] = fake()->name;
        }

        return [
            'name' => fake()->name(),
            'additional_names' => $additionalNames,
            'user_id' => User::factory(),
        ];
    }
}
