<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\CityNamesEnum;
use Closure;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\UnauthorizedException;

class GetNearestPostOfficeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'limit' => [
                function (string $attribute, mixed $value, Closure $fail) {
                    $citiesCount = count(CityNamesEnum::cases());
                    if ($value > $citiesCount) {
                        $fail(':attribute может быть не больше ' . $citiesCount);
                    }
                },
            ]
        ];
    }
}
