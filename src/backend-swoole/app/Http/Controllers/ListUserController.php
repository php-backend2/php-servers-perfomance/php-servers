<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use App\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ListUserController extends BaseController
{
    public function __invoke(int $limit): JsonResponse
    {
        $users = User::query()
            ->limit($limit)
            ->inRandomOrder()
            ->with(['socks', 'city'])
            ->get();

        return new JsonResponse([
            'data' => UserResource::collection($users),
        ]);
    }
}
