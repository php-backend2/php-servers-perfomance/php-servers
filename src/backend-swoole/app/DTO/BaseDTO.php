<?php

declare(strict_types=1);

namespace App\DTO;

class BaseDTO
{
    public function toArray(): array
    {
        return (array) $this;
    }
}
