<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $name
 * @property string $additional_names
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property int $user_id
 * @property User $user
 */
class UserSock extends Model
{
    use HasFactory;

    protected $casts = [
        'additional_names' => 'array',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function colors(): HasMany
    {
        return $this->hasMany(SockColor::class, 'sock_id', 'id');
    }
}
