<?php

declare(strict_types=1);

namespace App\Enums;

enum CityNamesEnum: string
{
    case TOMSK = 'Томск';
    case NOVOSIBIRSK = 'Новосибирск';
    case KRASNOYARSK = 'Красноярск';
    case OMSK = 'Омск';
    case MOSCOW = 'Москва';
}
