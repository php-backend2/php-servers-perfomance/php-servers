<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\SockColor;
use App\Resources\SockColorResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ListSockColorController extends BaseController
{
    public function __invoke(int $limit): JsonResponse
    {
        $sockColors = SockColor::query()
            ->limit($limit)
            ->inRandomOrder()
            ->with(['sock'])
            ->get();

        return new JsonResponse([
            'data' => SockColorResource::collection($sockColors),
        ]);
    }
}
