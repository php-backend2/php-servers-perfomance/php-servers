<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\UserSock;
use App\Resources\UserSockResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ListUserSockController extends BaseController
{
    public function __invoke(int $limit): JsonResponse
    {
        $userSocks = UserSock::query()
            ->limit($limit)
            ->inRandomOrder()
            ->with(['user', 'colors'])
            ->get();

        return new JsonResponse([
            'data' => UserSockResource::collection($userSocks),
        ]);
    }
}
