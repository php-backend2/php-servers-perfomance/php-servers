<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $name
 * @property string $additional_names
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property int $sock_id
 * @property UserSock $sock
 */
class SockColor extends Model
{
    use HasFactory;

    protected $casts = [
        'additional_names' => 'array',
    ];

    public function sock(): BelongsTo
    {
        return $this->belongsTo(UserSock::class, 'sock_id', 'id');
    }
}
