#!/bin/sh

if [ "${1}" != "" ]; then exec "$@"; fi

if [ ! -d "/app/vendor" ]; then
    echo "No such directory: /app/vendor. Installing composer packages" ;
    composer install --no-progress --ansi --no-scripts;
else
    echo "Nothing to install, update or remove: /app/vendor already exists";
fi

php artisan migrate --seed --force;

exec php artisan octane:start --watch --host=0.0.0.0 --port=9000 --workers=1 --task-workers=1 --max-requests=100;
