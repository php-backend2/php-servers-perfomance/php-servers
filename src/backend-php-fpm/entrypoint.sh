#!/bin/sh

if [ "${1}" != "" ]; then exec "$@"; fi

if [ ! -d "/app/vendor" ]; then
    echo "No such directory: /app/vendor. Installing composer packages" ;
    composer install --no-progress --ansi --no-scripts;
else
    echo "Nothing to install, update or remove: /app/vendor already exists";
fi

php artisan migrate --seed --force;

exec php-fpm --nodaemonize;
