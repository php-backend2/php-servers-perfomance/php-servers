<?php

return [
    'token' => (string) env('DADATA_TOKEN'),
    'secret' => (string) env('DADATA_SECRET'),
    'timeout'   => 10,
];
