<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use App\Models\SockColor;
use App\Models\User;
use App\Models\UserSock;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        if (User::count() > 10) return;

        $countries = Country::factory(5)->create();
        $cities = City::factory(10)
            ->state(new Sequence(
                fn(Sequence $sequence) => [
                    'country_id' => $countries->random()->id
                ]
            ))
            ->create();

        $users = User::factory(1000)
            ->state(new Sequence(
                fn (Sequence $sequence) => [
                    'city_id' => $cities->random()->id
                ],
            ))
            ->create();

        $userSocks = UserSock::factory(5000)
            ->state(new Sequence(
                fn (Sequence $sequence) => [
                    'user_id' => $users->random()->id
                ],
            ))
            ->create();

        SockColor::factory(50)
            ->state(new Sequence(
                fn (Sequence $sequence) => [
                    'sock_id' => $userSocks->random()->id
                ],
            ))
            ->create();
    }
}
