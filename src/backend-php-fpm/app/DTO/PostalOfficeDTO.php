<?php

namespace App\DTO;

class PostalOfficeDTO extends BaseDTO
{
    public function __construct(
        public readonly string|null $postal_code = null,
        public readonly bool|null   $is_closed = null,
        public readonly string|null $type_code = null,
        public readonly string|null $address_str = null,
        public readonly string|null $address_kladr_id = null,
        public readonly string|null $address_qc = null,
        public readonly float|null  $geo_lat = null,
        public readonly float|null  $geo_lon = null,
        public readonly string|null $schedule_mon = null,
        public readonly string|null $schedule_tue = null,
        public readonly string|null $schedule_wed = null,
        public readonly string|null $schedule_thu = null,
        public readonly string|null $schedule_fri = null,
        public readonly string|null $schedule_sat = null,
        public readonly string|null $schedule_sun = null,
    )
    {
    }

    public static function fromDaDataInfo(array $data): self
    {
        return new self(...$data);
    }
}
