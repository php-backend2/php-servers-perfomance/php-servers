<?php

declare(strict_types=1);

namespace App\Rules;

use App\Enums\CityNamesEnum;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CitiesCountRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $citiesCount = count(CityNamesEnum::cases());
        if ($value > $citiesCount) {
            $fail(':attribute может быть не больше ' . $citiesCount);
        }
    }
}
