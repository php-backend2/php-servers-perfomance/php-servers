<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\PostalOfficeDTO;
use App\Enums\CityNamesEnum;
use App\Http\Requests\GetNearestPostOfficeRequest;
use App\Resources\PostalOfficeResource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class GetNearestPostOfficeAction extends BaseController
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(GetNearestPostOfficeRequest $request)
    {
        $addresses = $this->getPostalOfficeCollection($request->get('limit'));
        return PostalOfficeResource::collection($addresses);
    }

    private function getPostalOfficeCollection(int|null $limit): Collection
    {
        $cityNames = collect(CityNamesEnum::cases());
        if ($limit) {
            $cityNames = $cityNames->take($limit);
        }

        return $cityNames->map(function (CityNamesEnum $cityNamesEnum) {
            /** @var array $address */
            $address = json_decode('{"suggestions":[{"value":"101000","unrestricted_value":"г Москва, ул Мясницкая, д 26А стр 1","data":{"postal_code":"101000","is_closed":false,"type_code":"ГОПС","address_str":"г Москва, ул Мясницкая, д 26А стр 1","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.763944,"geo_lon":37.637281,"schedule_mon":"00:00-00:00","schedule_tue":"00:00-00:00","schedule_wed":"00:00-00:00","schedule_thu":"00:00-00:00","schedule_fri":"00:00-00:00","schedule_sat":"00:00-00:00","schedule_sun":"00:00-00:00"}},{"value":"101300","unrestricted_value":"г Москва, ул Мясницкая, д 26А стр 1","data":{"postal_code":"101300","is_closed":true,"type_code":"ТИ","address_str":"г Москва, ул Мясницкая, д 26А стр 1","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.763944,"geo_lon":37.637281,"schedule_mon":null,"schedule_tue":null,"schedule_wed":null,"schedule_thu":null,"schedule_fri":null,"schedule_sat":null,"schedule_sun":null}},{"value":"101700","unrestricted_value":"г Москва, ул Мясницкая, д 26А стр 1","data":{"postal_code":"101700","is_closed":false,"type_code":"УФПС","address_str":"г Москва, ул Мясницкая, д 26А стр 1","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.763944,"geo_lon":37.637281,"schedule_mon":"09:00-18:00, обед 12:00-12:45","schedule_tue":"09:00-18:00, обед 12:00-12:45","schedule_wed":"09:00-18:00, обед 12:00-12:45","schedule_thu":"09:00-18:00, обед 12:00-12:45","schedule_fri":"09:00-16:45, обед 12:00-12:45","schedule_sat":null,"schedule_sun":null}},{"value":"101702","unrestricted_value":"г Москва, ул Краснопрудная, д 3\/5 стр 2","data":{"postal_code":"101702","is_closed":true,"type_code":"CТМЦ","address_str":"г Москва, ул Краснопрудная, д 3\/5 стр 2","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.77757,"geo_lon":37.660197,"schedule_mon":null,"schedule_tue":null,"schedule_wed":null,"schedule_thu":null,"schedule_fri":null,"schedule_sat":null,"schedule_sun":null}},{"value":"101721","unrestricted_value":"г Москва, ул Маршала Бирюзова, д 2","data":{"postal_code":"101721","is_closed":false,"type_code":"УЧАСТОК","address_str":"г Москва, ул Маршала Бирюзова, д 2","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.790327,"geo_lon":37.501348,"schedule_mon":"08:00-18:00, обед 13:00-14:00","schedule_tue":"08:00-18:00, обед 13:00-14:00","schedule_wed":"08:00-18:00, обед 13:00-14:00","schedule_thu":"08:00-18:00, обед 13:00-14:00","schedule_fri":"08:00-18:00, обед 13:00-14:00","schedule_sat":null,"schedule_sun":null}},{"value":"101722","unrestricted_value":"г Москва, Марушкинское п, кв-л 63, д 1 стр 34","data":{"postal_code":"101722","is_closed":false,"type_code":"УЧАСТОК","address_str":"г Москва, Марушкинское п, кв-л 63, д 1 стр 34","address_kladr_id":"77009000000005000","address_qc":"0","geo_lat":55.581103,"geo_lon":37.189777,"schedule_mon":"08:00-18:00, обед 13:00-14:00","schedule_tue":"08:00-18:00, обед 13:00-14:00","schedule_wed":"08:00-18:00, обед 13:00-14:00","schedule_thu":"08:00-18:00, обед 13:00-14:00","schedule_fri":"08:00-18:00, обед 13:00-14:00","schedule_sat":null,"schedule_sun":null}},{"value":"101760","unrestricted_value":"г Москва, ул Перовская, д 68","data":{"postal_code":"101760","is_closed":false,"type_code":"УЧАСТОК","address_str":"г Москва, ул Перовская, д 68","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.743401,"geo_lon":37.809012,"schedule_mon":"00:00-00:00","schedule_tue":"00:00-00:00","schedule_wed":"00:00-00:00","schedule_thu":"00:00-00:00","schedule_fri":"00:00-00:00","schedule_sat":"00:00-00:00","schedule_sun":"00:00-00:00"}},{"value":"101790","unrestricted_value":"г Москва, ул Мясницкая, д 26А стр 1","data":{"postal_code":"101790","is_closed":false,"type_code":"ТИ","address_str":"г Москва, ул Мясницкая, д 26А стр 1","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.763944,"geo_lon":37.637281,"schedule_mon":"09:00-18:00","schedule_tue":"09:00-18:00","schedule_wed":"09:00-18:00","schedule_thu":"09:00-18:00","schedule_fri":"09:00-18:00","schedule_sat":null,"schedule_sun":null}},{"value":"101976","unrestricted_value":"г Москва, ул Перовская, д 68","data":{"postal_code":"101976","is_closed":true,"type_code":"ТИ","address_str":"г Москва, ул Перовская, д 68","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.743401,"geo_lon":37.809012,"schedule_mon":null,"schedule_tue":null,"schedule_wed":null,"schedule_thu":null,"schedule_fri":null,"schedule_sat":null,"schedule_sun":null}},{"value":"101990","unrestricted_value":"г Москва, ул Мясницкая, д 26А стр 1","data":{"postal_code":"101990","is_closed":false,"type_code":"ГСП","address_str":"г Москва, ул Мясницкая, д 26А стр 1","address_kladr_id":"7700000000000","address_qc":"0","geo_lat":55.763944,"geo_lon":37.637281,"schedule_mon":"08:00-17:00","schedule_tue":"08:00-17:00","schedule_wed":"08:00-17:00","schedule_thu":"08:00-17:00","schedule_fri":"08:00-17:00","schedule_sat":null,"schedule_sun":null}}]}', true);
            $nearestPostal = Arr::random(Arr::random($address));
            $data = Arr::get($nearestPostal, 'data');
            return PostalOfficeDTO::fromDaDataInfo($data);
        });
    }
}
