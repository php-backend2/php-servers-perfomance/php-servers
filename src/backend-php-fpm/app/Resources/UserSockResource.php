<?php

declare(strict_types=1);

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\UserSock
 */
class UserSockResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'additional_names' => $this->additional_names,
            'user' => UserResource::make($this->whenLoaded('user')) ?? null,
            'colors' => SockColorResource::collection($this->whenLoaded('colors')),
            'created_at' => $this->created_at->toDateString(),
            'updated_at' => $this->updated_at->toDateString(),
        ];
    }
}
