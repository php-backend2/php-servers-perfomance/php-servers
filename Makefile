DOCKER_COMPOSE:=docker compose

BASE_COMPOSE_FILE:=COMPOSE_FILE=docker-compose.yml:docker-compose.local.yml
BASE_COMPOSE_PROFILE:=COMPOSE_PROFILES=general
BASE_COMPOSE_ARGS:=$(BASE_COMPOSE_FILE) BACKEND_BUILD_TARGET=server-local

SWOOLE_ARGS:=$(BASE_COMPOSE_PROFILE),swoole BACKEND_SRC_DIR=backend-swoole BACKEND_PHP_SERVER=swoole
ROADRUNNER_ARGS:=$(BASE_COMPOSE_PROFILE),roadrunner BACKEND_SRC_DIR=backend-roadrunner BACKEND_PHP_SERVER=roadrunner
PHP_FPM_ARGS:=$(BASE_COMPOSE_PROFILE),php-fpm BACKEND_SRC_DIR=backend-php-fpm BACKEND_PHP_SERVER=php-fpm

SWOOLE:=$(BASE_COMPOSE_ARGS) $(SWOOLE_ARGS) $(DOCKER_COMPOSE)
ROADRUNNER:=$(BASE_COMPOSE_ARGS) $(ROADRUNNER_ARGS) $(DOCKER_COMPOSE)
PHP_FPM:=$(BASE_COMPOSE_ARGS) $(PHP_FPM_ARGS) $(DOCKER_COMPOSE)
MONITOR:=DOCKER_BUILDKIT=1 COMPOSE_PROFILES=monitoring $(DOCKER_COMPOSE)

swoole-build:
	$(SWOOLE) --progress=plain build --no-cache
swoole-upd:
	$(SWOOLE) up -d
swoole-down:
	$(SWOOLE) down --remove-orphans --timeout 0
swoole-logs:
	$(SWOOLE) logs

roadrunner-build:
	$(ROADRUNNER) --progress=plain build --no-cache
roadrunner-upd:
	$(ROADRUNNER) up -d
roadrunner-down:
	$(ROADRUNNER) down --remove-orphans --timeout 0
roadrunner-logs:
	$(ROADRUNNER) logs

php-fpm-build:
	$(PHP_FPM) --progress=plain build --no-cache
php-fpm-upd:
	$(PHP_FPM) up -d
php-fpm-down:
	$(PHP_FPM) down --remove-orphans --timeout 0
php-fpm-logs:
	$(PHP_FPM) logs

monitor-build:
	$(MONITOR) --progress=plain build --no-cache
monitor-upd:
	$(MONITOR) up -d
monitor-down:
	$(MONITOR) down --remove-orphans --timeout 0
monitor-logs:
	$(MONITOR) logs
